# Mars Rover Deployer

## Description
This is the Mars Rover Deployer project, built with NodeJS, Docker and Jest.
Docker is used to run this project to ensure Node version is compatible with the features. Yarn will lift the container and run the app.

## How to run
Please, make sure you have Docker installed. Then, just run ``yarn start`` and see the magic happen. You'll be able to see the output on the console.
In order to change the number and information of rovers being deployed, edit the ``rovers.js`` file at the root of the project. Then run the app again using ``yarn start``

## How to run Unit Tests
Jest is used to implement the tests here. But before you run any tests, you need to install dependencies. Before running tests, run ``yarn install`` to make sure you have the dependencies locally.
Just run ``yarn test`` and you should see the test results on the console.
