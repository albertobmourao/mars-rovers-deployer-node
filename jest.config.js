export default {
  transform: {},
  roots: [
    "./"
  ],
  modulePaths: [
    "./"
  ],
  moduleDirectories: [
    "node_modules"
  ],
  moduleNameMapper: {
    "#/": "../../src/",
    "#models": "../../src/models/index.js",
    "#helpers": "../../src/helpers/index.js",
    "#consts": "../../src/consts/index.js"
  } 
}