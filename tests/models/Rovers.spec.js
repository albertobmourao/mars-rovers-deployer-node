import { Rover } from '#models/Rover.js'

describe('Rover model', () => {
  describe('deploy method', ()=> {
    it('should deploy a rover and return its final position.', async () => {
      const rover = new Rover('Rover test', '1 2 N', 'LMLMLMLMM')

      const finalPosition = await rover.deploy(true)

      expect(finalPosition).toEqual('1 3 N')
    })
  })

  describe('spin method', ()=> {
    it('should spin a Rover from North to South', () => {
      const rover = new Rover('Rover test', '0 0 N', 'LL')

      rover.spin('L')
      rover.spin('L')

      expect(rover.direction).toEqual('S')
    })
  })

  describe('move method', ()=> {
    it('should move a Rover forward 2 positions', () => {
      const rover = new Rover('Rover test', '0 0 N', 'MM')

      rover.move()
      rover.move()

      expect(rover.currentPosition).toEqual('0 2 N')
    })
  })
})
