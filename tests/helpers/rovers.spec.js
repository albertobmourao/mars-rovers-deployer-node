import { instantiateRovers } from '#helpers'
import { roversArray } from './mocks.js'
import { Rover } from '#models/Rover.js'

describe('Helpers', () => {
  describe('instantiateRovers helper', () => {
    it('should instantiate an array of rovers with Rover class.', () => {
      const roversInstances = instantiateRovers(roversArray)

      expect(roversInstances).toEqual([
        new Rover('Rover 1', '1 2 N', 'LMLMLMLMM'),
        new Rover('Rover 2', '3 3 E', 'MRRMMRMRRM')
      ])
    })
  })
})
