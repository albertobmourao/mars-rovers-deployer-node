import { instantiateRovers } from '#helpers'

export const roversArray = [
  {
    name: 'Rover 1',
    initialPosition: '1 2 N',
    movements: 'LMLMLMLMM'
  },
  {
    name: 'Rover 2',
    initialPosition: '3 3 E',
    movements: 'MRRMMRMRRM'
  }
]

export const rovers = instantiateRovers(roversArray)
