import { calculateTimeouts } from '#helpers'
import { rovers } from './mocks.js'

describe('Helpers', () => {
  describe('calculateTimeouts helper', () => {
    it('should calculate the timeout before each rover is deployed.', () => {
      const timeouts = calculateTimeouts(rovers)

      expect(timeouts).toEqual([ 0, 4500 ])
    })
  })
})
