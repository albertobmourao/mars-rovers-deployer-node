import { defaultTimeout } from '#consts'

export class Rover {
  // declare protected properties
  #direction
  #positionX
  #positionY

  constructor (name, initialPosition, movements) {
    const [ positionX, positionY, direction ] = initialPosition.split(' ')

    this.#direction = direction
    this.#positionX = Number(positionX)
    this.#positionY = Number(positionY)
    this._name = name
    this._movements = movements.split('')
  }

  get currentPosition () {
    return `${this.#positionX} ${this.#positionY} ${this.#direction}`
  }

  get movements () {
    return this._movements
  }

  get direction () {
    return this.#direction
  }

  spin (movement) {
    const directions = {
      N: {
        L: 'W',
        R: 'E'
      },
      E: {
        L: 'N',
        R: 'S'
      },
      W: {
        L: 'S',
        R: 'N'
      },
      S: {
        L: 'E',
        R: 'W'
      }
    }

    // Use direction of the movement to find which point Rover is now facing based on its previous direction
    this.#direction = directions[this.#direction][movement]
  }

  move () {
    switch (this.#direction) {
      case 'N':
        this.#positionY = this.#positionY + 1
        break
      case 'E':
        this.#positionX = this.#positionX + 1
        break
      case 'S':
        this.#positionY = this.#positionY - 1
        break
      case 'W':
        this.#positionX = this.#positionX - 1
        break
    }
  }

  deploy (testMode = false) {
    // stop console from flooding with unnecessary messages when testing
    if (testMode) console.log = () => {}

    // return a Promise so that unit tests can properly get final position
    return new Promise(resolve => {
      // generate a random color number for each rover in the console
      const roverColor = Math.abs(Math.floor(Math.random() * 10 - 3))

      this._movements.forEach((command, index) => {
        if (index === 0) {
          console.log(`\x1b[4${roverColor}m`, this._name, '\x1b[0m', `is being deployed...`)
          console.log(`Initial position: ${this.currentPosition}`)
          console.log(`Movements:`)
        }
  
        setTimeout(() => {
          const isLast = index + 1 === this._movements.length
          command === 'M' ? this.move() : this.spin(command)
          
          if (isLast) {
            console.log(`\x1b[4${roverColor}m`, this._name,  '\x1b[0m', `successfully deployed here:`)
            console.log('\x1b[42m', this.currentPosition, '\x1b[0m')
            console.log('===============')
            resolve(this.currentPosition)
            return
          }
  
          console.log(this.currentPosition)
        }, testMode ? 0 : (index * defaultTimeout)) // use no timeout for Unit Tests to avoid unnecessary delay
      })
    })
  }
}
