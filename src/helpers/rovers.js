import { Rover } from '#models'

export const instantiateRovers = (rovers) => (
  rovers.map(({ name, initialPosition, movements }) => (
    new Rover(name, initialPosition, movements)
  ))
)
