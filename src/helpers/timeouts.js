import { defaultTimeout } from '#consts'

export const calculateTimeouts = (rovers) => {
  // calculate the timeout each rover willl have to wait before being deployed
  let totalTimeouts = 0

  return rovers.map((rover, index) => {
    if (index === 0) {
      totalTimeouts = rover.movements.length * defaultTimeout
      return index
    }
  
    const time = rover.movements.length * defaultTimeout
    totalTimeouts += time
    return totalTimeouts - time
  })
}
