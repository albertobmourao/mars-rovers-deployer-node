// Add as many rovers as you want to deploy
export default [
  {
    name: 'Rover 1',
    initialPosition: '1 2 N',
    movements: 'LMLMLMLMM'
  },
  {
    name: 'Rover 2',
    initialPosition: '3 3 E',
    movements: 'MRRMMRMRRM'
  }
]
