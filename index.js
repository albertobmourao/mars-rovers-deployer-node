import { calculateTimeouts, instantiateRovers } from '#helpers'
import roversArray from './rovers.js'

const rovers = instantiateRovers(roversArray)
const timeouts = calculateTimeouts(rovers)

rovers.forEach((rover, index) => {
  setTimeout(() => {
    rover.deploy()
  }, timeouts[index])
})
