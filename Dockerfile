FROM node:16-alpine

WORKDIR /app

COPY . .

EXPOSE 80

CMD ["node", "index.js"]
